'Use Strict';

angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});

angular.module('App', ['ionic','ngStorage', 'ngCordova','firebase', 'ngMessages', 'ionic', 'angularMoment', 'underscore', 'ngMap', 'ngResource',
  'App.filters',
  'App.factories',
  'App.directives',
  'App.views',
  'slugifier',
  'ionic.contrib.ui.tinderCards',
  'youtube-embed'])


  .run(function($ionicPlatform, $rootScope, $ionicConfig, $firebaseAuth, $firebase, $timeout) {

    $ionicPlatform.on("deviceready", function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)


      if(window.cordova && window.cordova.plugins.AdMob && window.cordova.plugins.Keyboard) {

      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }

      PushNotificationsService.register();
    });

    // This fixes transitions for transparent background views
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
      if(toState.name.indexOf('auth.walkthrough-welcome') > -1)
      {
        // set transitions to android to avoid weird visual effect in the walkthrough transitions
        $timeout(function(){
          $ionicConfig.views.transition('android');
          $ionicConfig.views.swipeBackEnabled(false);
          console.log("setting transition to android and disabling swipe back");
        }, 0);
      }
    });
    $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams){
      if(toState.name.indexOf('app.categories') > -1)
      {
        // Restore platform default transition. We are just hardcoding android transitions to auth views.
        $ionicConfig.views.transition('platform');
        // If it's ios, then enable swipe back again
        if(ionic.Platform.isIOS())
        {
          $ionicConfig.views.swipeBackEnabled(true);
        }
        console.log("enabling swipe back and restoring transition to platform default", $ionicConfig.views.transition());
      }
    });



    $ionicPlatform.on("resume", function(){
      PushNotificationsService.register();
    });


  })


.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

$stateProvider

//abstract template

.state('auth', {
      url: "/auth",
      templateUrl: "views/auth/auth.html",
      abstract: true,
      controller: 'AuthCtrl'
  })

  .state('auth.walkthrough-welcome', {
  url: '/walkthrough-welcome',
  templateUrl: 'views/getting_started/welcome.html',
  controller:'GettingStartedCtrl'
})


  .state('auth.walkthrough-learn', {
    url: '/walkthrough-learn',
    templateUrl: 'views/getting_started/learn.html',
    controller:'GettingStartedCtrl'
  })

.state('auth.walkthrough', {
      url: '/walkthrough',
      templateUrl: 'views/walkthrough/walkthrough.html',
      })

    .state('auth.login', {
      url: '/login',
      templateUrl: 'views/login/login.html',
      controller:'loginController'
    })
    .state('auth.forgot', {
      url: '/forgot',
      templateUrl: 'views/forgot/forgot.html',
      controller:'forgotController'
    })
    .state('auth.register', {
      url: '/register',
      templateUrl: 'views/register/register.html',
      controller:'registerController'
    })


    .state('app', {
          url: "/app",
          abstract: true,
          templateUrl: "views/sidemenu/sidemenu.html",
          controller: 'AppCtrl'
        })

    .state('app.profile', {
          url: '/profile',
          templateUrl: 'views/profile/profile.html',
          controller:'homeController'
        })

        .state('app.support', {
              url: "/support",
              views: {
                'menuContent': {
                  templateUrl: "views/supportt/support.html",

                }
              }
            })

        .state('app.settings', {
        url: "/settings",
        views: {
        'menuContent': {
          templateUrl: "views/settings/settings.html",
          controller: 'SettingsCtrl'
        }
        }
        })


.state('app.categories', {
  url: "/categories",
  views: {
    'menuContent': {
      templateUrl: "views/categories/categories.html",
      controller: 'FeedsCategoriesCtrl'
    }
  }
})

.state('app.subcategories', {
  url: "/subcategories/:categoryId",
  views: {
    'menuContent': {
      templateUrl: "views/subcategories/subcategories.html",
      controller: 'subcategoriesCtrl'
    }
  }
})

.state('app.events_in_subcategories', {
  url: "/events_in_subcategories/:categoryId",
  views: {
    'menuContent': {
      templateUrl: "views/events_in_subcategories/events_in_subcategories.html",
      controller: 'events_in_subcategoriesCtrl'
    }
  }
})

.state('app.lists', {
url: "/lists",
views: {
'menuContent': {
  templateUrl: "views/lists/lists.html",
  controller: 'listsCtrl'
}
}
})

.state('app.myevents', {
url: "/myevents",
views: {
'menuContent': {
  templateUrl: "views/myevents/myevents.html",
  controller: 'myeventsCtrl'
}
}
})

.state('app.layouts', {
  url: "/layouts",
  views: {
    'menuContent': {
      templateUrl: "views/layouts/layouts.html"
    }
  }
})

.state('app.image-picker', {
    url: "/random/image-picker",
    views: {
      'menuContent': {
        templateUrl: "views/random/image-picker.html",
        controller: 'ImagePickerCtrl'
      }
    }
  })


.state('app.tinder-cards', {
  url: "/layouts/tinder-cards",
  views: {
    'menuContent': {
      templateUrl: "views/layouts/tinder-cards.html",
      controller: 'TinderCardsCtrl'
    }
  }
})

.state('app.slider', {
      url: "/layouts/slider",
      views: {
        'menuContent': {
          templateUrl: "views/layouts/slider.html"
        }
      }
    })

.state('app.bookmarks', {
      url: "/bookmarks",
      views: {
        'menuContent': {
          templateUrl: "views/bookmarks/bookmarks.html",
          controller: 'BookMarksCtrl'
        }
      }
    })

  .state('app.forms', {
        url: "/forms",
        views: {
          'menuContent': {
            templateUrl: "views/forms/forms.html",
                controller: 'formsCtrl'
          }
        }
      })

.state('app.maps', {
url: "/maps",
views: {
'menuContent': {
  templateUrl: "views/maps/maps.html",
  controller: 'MapsCtrl'
}
}
});

$urlRouterProvider.otherwise('auth/walkthrough-welcome');

//$timeout(function() {
  //  $state.go('auth.walkthrough');
//}, 5000);
})
// Changue this for your Firebase App URL.

.constant('FURL', {
  apiKey: "AIzaSyBJiyUD47D8v28udXh60vKwhz0GPMC8OmU",
  authDomain: "dmv-social.firebaseapp.com",
  databaseURL: "https://dmv-social.firebaseio.com",
  storageBucket: "dmv-social.appspot.com",

  })
/*
  .constant('FURLL', {
    apiKey: "AIzaSyDZXuGrMD4rQba6h4iLCw1f3byZLoH9HAk",
    authDomain: "dmv-social-f9265.firebaseapp.com",
    databaseURL: "https://dmv-social-f9265.firebaseio.com",
    storageBucket: "dmv-social-f9265.appspot.com",
    messagingSenderId: "338788331643"
    })
*/
//random controllers
  .controller('RateApp', function($scope) {
  	$scope.rateApp = function(){
  		if(ionic.Platform.isIOS()){
  			//you need to set your own ios app id
  			AppRate.preferences.storeAppURL.ios = '1234555553>';
  			AppRate.promptForRating(true);
  		}else if(ionic.Platform.isAndroid()){
  			//you need to set your own android app id
  			AppRate.preferences.storeAppURL.android = 'market://details?id=ionFB';
  			AppRate.promptForRating(true);
  		}
  	};
  })

  .controller('ImagePickerCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaCamera) {

	$scope.images = [];
	// $scope.image = {};

	// $scope.openImagePicker = function() {
	//
	// 	//We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
  //   //implemented for iOS and Android 4.0 and above.
	//
  //   $ionicPlatform.ready(function() {
  //     $cordovaImagePicker.getPictures()
  //      .then(function (results) {
  //         for (var i = 0; i < results.length; i++) {
  //           console.log('Image URI: ' + results[i]);
  //           $scope.images.push(results[i]);
  //         }
  //       }, function(error) {
  //         // error getting photos
  //       });
  //   });
	// };

	$scope.openImagePicker = function(){
    //We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
    //implemented for iOS and Android 4.0 and above.

    $ionicPlatform.ready(function() {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 350,
        targetHeight: 350,
        saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options)
       .then(function (imageData) {
          var image = "data:image/jpeg;base64," + imageData;
          $scope.images.push(image);
        }, function(error) {
          console.log(error);
        });
    });
  };

	$scope.removeImage = function(image) {
		$scope.images = _.without($scope.images, image);
	};


	$scope.shareImage = function(image) {
		window.plugins.socialsharing.share(null, null, image);
	};


	$scope.shareAll = function() {
		window.plugins.socialsharing.share(null, null, $scope.images);
	};

//var $scope.addImage = function(image) {
  		//window.plugins.socialsharing.share(null, null, image);
  	//};

  $scope.addAll = function(image) {
		window.plugins.socialsharing.share(null, null, image);
	};
})

.controller('AdsCtrl', function($scope, $ionicActionSheet, AdMob, iAd) {

  var admob_ios_key = 'ca-app-pub-4136693848769819~9448751483';
  var admob_android_key = 'ca-app-pub-4136693848769819~7112563888';
  var adId = (navigator.userAgent.indexOf('Android') >=0) ? admob_android_key : admob_ios_key;

$scope.manageAdMo = function() {

        AdMob.showBanner();
      };

$scope.manageiAd = function() {
      iAd.showBanner();
      };
})

.constant('GCM_SENDER_ID', '574597432927')

  .controller('SendMailCtrl', function($scope, $cordovaEmailComposer, $ionicPlatform) {
    //we use email composer cordova plugin, see the documentation for mor options: http://ngcordova.com/docs/plugins/emailComposer/
    $scope.sendMail = function(){
      $ionicPlatform.ready(function() {
        $cordovaEmailComposer.isAvailable().then(function() {
          // is available
          console.log("Is available");
          $cordovaEmailComposer.open({
            to: 'dmvsocial@gmail.com',
            subject: 'Customer Support',
    				body:    ''
          }).then(null, function () {
            // user cancelled email
          });
        }, function () {
          // not available
          console.log("Not available");
        });
      });
    };
  });
