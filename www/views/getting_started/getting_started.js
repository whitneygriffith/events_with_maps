'Use Strict';
angular.module('App')

.controller('GettingStartedCtrl', function($scope, $state, $ionicSlideBoxDelegate) {
	$scope.skipIntro = function(){
		$state.go('auth.register');
	};

	$scope.show_call_to_action_button = false;
	$scope.show_skip_button = false;

	var slides = $ionicSlideBoxDelegate.$getByHandle('getting-started-slides');

	$scope.pagerClicked = function(index){
		slides.slide(index);
	};

	$scope.slideChanged = function($index){
		if(($index+1) === slides.slidesCount())
		{
			// We are in the last slide, show "Sign Up" call to action button
			$scope.show_call_to_action_button = true;
			slides.update();
		}
		else
		{
			$scope.show_call_to_action_button = false;
		}

		// Show skip on every slide except the firts one
		if(($index+1) > 1)
		{
			$scope.show_skip_button = true;
		}
		else
		{
			$scope.show_skip_button = false;
		}
	};

	// Getting started questions
	$scope.browsing = 'Parties';

	$scope.pick_categories = {};
	$scope.pick_categories.college_parties = true;
	$scope.pick_categories.turnups = true;

})

.directive('customCheckbox', function($ionicConfig) {
  return {
    restrict: 'E',
		scope: {
			title: '@',
			model: '=ngModel'
		},
    replace: true,
    transclude: true,
    templateUrl: 'views/getting_started/custom-checkbox.template.html',
    compile: function(element, attr) {
      var checkboxWrapper = element[0].querySelector('.checkbox');
      checkboxWrapper.classList.add('checkbox-' + $ionicConfig.form.checkbox());
    }
  };
})

.service('TestService', function ($http, $q){
  this.testMethod = function(){

  };
})
;
