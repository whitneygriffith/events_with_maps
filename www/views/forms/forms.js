'Use Strict';
angular.module('App')


.controller('formsCtrl', function($scope, FURL, $firebaseArray, $firebaseObject, $ionicLoading, $state, $rootScope, $ionicPlatform, $cordovaCamera) {

	console.log("My events worked");

	var fbAuth = firebase.getAuth;
	if(fbAuth) {
		 var userReference = firebase.child("users/" + fbAuth.uid);
		 var syncArray = $firebaseArray(userReference.child("eventImg"));
		 $scope.images = syncArray;
 } else {
		 console.log('sign in error');
 }

	$scope.images = [];

	$scope.openImagePicker = function(){
		//We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
		//implemented for iOS and Android 4.0 and above.

		$scope.upload = function() {
			var options = {
				quality: 100,
				destinationType: Camera.DestinationType.DATA_URL,
				sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
				allowEdit: true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 350,
				targetHeight: 350,
				saveToPhotoAlbum: false
			};

			$cordovaCamera.getPicture(options)
			 .then(function (imageData) {
				 syncArray.$add({img: imageData}).then(function() {
							 alert("Image has been uploaded");
					 });
				}, function(error) {
					console.log(error);
				});
	};

	$scope.removeImage = function(image) {
		$scope.images = _.without($scope.images, image);
	};

};

})
;
//https://www.airpair.com/ionic-framework/posts/ionic-firebase-camera-app
