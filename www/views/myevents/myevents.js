'Use Strict';
angular.module('App')

.controller('myeventsCtrl', function($scope, FURL, $rootScope, $window, $ionicModal, $firebaseArray, $firebaseObject, $ionicLoading, $state) {

console.log("My events worked");
$rootScope.checkSession = function() {
      var auth = new FirebaseSimpleLogin(authRef, function(error, user) {
        if (error) {
          // no action yet.. redirect to default route
          $rootScope.userEmail = null;
          $window.location.href = '#/authwalkthrough';
        } else if (user) {
          // user authenticated with Firebase
          $rootScope.userEmail = user.email;
          $window.location.href = ('#/categories');
        } else {
          // user is logged out
          $rootScope.userEmail = null;
          $window.location.href = '#/app/myevents';
        }
      });


        $rootScope.userEmail = null;
        $rootScope.baseUrl = 'https://dmv-social.firebaseio.com';
        var authRef = new Firebase($rootScope.baseUrl);
        $rootScope.auth = $firebaseAuth(authRef);


$rootScope.show("Please wait... Processing");
 $scope.list = [];
 var myeventsRef = new Firebase($rootScope.baseUrl + escapeEmailAddress($rootScope.userEmail));
 myeventsRef.on('value', function(snapshot) {
   var data = snapshot.val();

   $scope.list = [];

   for (var key in data) {
     if (data.hasOwnProperty(key)) {
       if (data[key].isCompleted == false) {
         data[key].key = key;
         $scope.list.push(data[key]);
       }
     }
   }

   if ($scope.list.length == 0) {
     $scope.noData = true;
   } else {
     $scope.noData = false;
   }
   $rootScope.hide();
 });

 $ionicModal.fromTemplateUrl('views/myevents/myevents.html', function(modal) {
   $scope.newTemplate = modal;
 });

 $scope.newTask = function() {
   $scope.newTemplate.show();
 };

 $scope.markCompleted = function(key) {
   $rootScope.show("Please wait... Updating List");
   var itemRef = new Firebase($rootScope.baseUrl + escapeEmailAddress($rootScope.userEmail) + '/' + key);
   itemRef.update({
     isCompleted: true
   }, function(error) {
     if (error) {
       $rootScope.hide();
       $rootScope.notify('Oops! something went wrong. Try again later');
     } else {
       $rootScope.hide();
       $rootScope.notify('Successfully updated');
     }
   });
 };

 $scope.deleteItem = function(key) {
   $rootScope.show("Please wait... Deleting from List");
   var itemRef = new Firebase($rootScope.baseUrl + escapeEmailAddress($rootScope.userEmail));
   myeventsRef.child(key).remove(function(error) {
     if (error) {
       $rootScope.hide();
       $rootScope.notify('Oops! something went wrong. Try again later');
     } else {
       $rootScope.hide();
       $rootScope.notify('Successfully deleted');
     }
   });
 };

}

function escapeEmailAddress(email) {
  if (!email) return false
  // Replace '.' (not allowed in a Firebase key) with ','
  email = email.toLowerCase();
  email = email.replace(/\./g, ',');
  return email.trim();
}


});
