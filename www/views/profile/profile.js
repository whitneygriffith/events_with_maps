'Use Strict';
angular.module('App')

.controller('homeController', function ($scope, $state,$cordovaOauth, $localStorage, $log, $location,$http,$ionicPopup, $firebaseObject, Auth, FURL, Utils) {

  var ref = firebase.database().ref();

$scope.image = 'img/profile.jpg';

var user = firebase.auth().currentUser;
var name, email, uid;
if (user !== null) {
  name = user.displayName;
  email = user.email;
  uid = user.uid;  // The user's ID, unique to the Firebase project. Do NOT use
                   // this value to authenticate with your backend server, if
                   // you have one. Use User.getToken() instead.
}

$scope.logOut = function () {
    Auth.logout();
    $location.path("/login");
};

});
