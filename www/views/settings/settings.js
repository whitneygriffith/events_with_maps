'Use Strict';
angular.module('App')

/*
.controller('SettingsCtrl', function($scope, $ionicActionSheet, $state) {

	// Triggered on a the logOut button click
  firebase.auth().signOut().then(function() {
  // Sign-out successful.
  // Show the action sheet
  var hideSheet = $ionicActionSheet.show({

    destructiveText: 'Logout',
    titleText: 'Are you sure you want to logout? This app is awesome so I reccommend that you to stay.',
    cancelText: 'Cancel',
    cancel: function() {
      // add cancel code..
    },
    buttonClicked: function(index) {
      //Called when one of the non-destructive buttons is clicked,
      //with the index of the button that was clicked and the button object.
      //Return true to close the action sheet, or false to keep it opened.
      return true;
    },
    destructiveButtonClicked: function(){
      //Called when the destructive button is clicked.
      //Return true to close the action sheet, or false to keep it opened.
      $state.go('walkthrough');
    }
  });
}, function(error) {
  // An error happened.
});
})
;
*/
.controller('SettingsCtrl', function ($scope, $state,$cordovaOauth, $localStorage, $log, $location,$http,$ionicPopup, $firebaseObject, Auth, FURL, Utils) {

  var ref = firebase.database().ref();

$scope.image = 'img/profile.jpg';

var user = firebase.auth().currentUser;
var name, email, uid;
if (user !== null) {
  name = user.displayName;
  email = user.email;
  uid = user.uid;  // The user's ID, unique to the Firebase project. Do NOT use
                   // this value to authenticate with your backend server, if
                   // you have one. Use User.getToken() instead.
}

$scope.logOut = function () {
    Auth.logout();
    $location.path("/login");
};

});
