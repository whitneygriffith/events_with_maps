'Use Strict';
angular.module('App')


.controller('subcategoriesCtrl', function($scope, $http, $stateParams) {
	$scope.category_sources = [];

  $scope.categoryId = $stateParams.categoryId;

	$http.get('feeds-categories.json').success(function(response) {
		var category = _.find(response, {id: $scope.categoryId});
		$scope.categoryTitle = category.title;
		$scope.category_sources = category.feed_sources;
	});
});

/*
$scope.category_sources = [];

	$scope.categoryId = $stateParams.categoryId;

	$http.get('feeds-categories.json').success(function(response) {
		var category = _.find(response, {id: $scope.categoryId});
	//	$scope.categoryTitle = category.title;
	//	$scope.category_sources = category.feed_sources;
	});
});
*/
