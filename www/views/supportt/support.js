'Use Strict';
angular.module('App')

.controller('RateApp', function($scope) {
  $scope.rateApp = function(){
    if(ionic.Platform.isIOS()){
      //you need to set your own ios app id
      AppRate.preferences.storeAppURL.ios = '1234555553>';
      AppRate.promptForRating(true);
    }else if(ionic.Platform.isAndroid()){
      //you need to set your own android app id
      AppRate.preferences.storeAppURL.android = 'https://play.google.com/store/apps/details?id=com.dmvsocial.android';
      AppRate.promptForRating(true);
    }
  };
})


.controller('SendMailCtrl', function($scope, $cordovaEmailComposer, $ionicPlatform) {
  //we use email composer cordova plugin, see the documentation for mor options: http://ngcordova.com/docs/plugins/emailComposer/
  $scope.sendMail = function(){
    $ionicPlatform.ready(function() {
      $cordovaEmailComposer.isAvailable().then(function() {
        // is available
        console.log("Is available");
        $cordovaEmailComposer.open({
          to: 'dmvsocial@gmail.com',
          subject: 'Customer Support',
          body:    ''
        }).then(null, function () {
          // user cancelled email
        });
      }, function () {
        // not available
        console.log("Not available");
      });
    });
  };
});
